package ru.pulkovo.taskmanager.checkups.sample;

import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;
import ru.pulkovo.taskmanager.manager.Query;
import ru.pulkovo.taskmanager.manager.TestBase;
import ru.pulkovo.taskmanager.model.FlightModel;
import ru.pulkovo.taskmanager.model.ServiceStandard;

import java.util.ArrayList;
import java.util.List;



public class Tests extends TestBase implements Query {

    public static void main(String[] args) {
        JSONArray request_services = app.request("test").getServices().getJSONObject("data").getJSONArray("serviceStandards");
        List<ServiceStandard> services = new ArrayList<>();
        Gson gson = new Gson();
        for(Object serv : request_services){
            ServiceStandard response = gson.fromJson(String.valueOf(serv), ServiceStandard.class);
            services.add(response);
        }
        for(ServiceStandard s : services){
            if(s.getName().equals("S20_21 test")){
                System.out.println("Сервис уже создан");
            }
        }
//        JSONObject res =  app.req().responseToString(app.req().postRequast(TEST,FLIGHT,app.req().createVariablesDay("2021","08","30")).body().toString());
//        JSONArray flight = res.getJSONObject("data").getJSONArray("serviceObjects");
//
//        List<FlightModel> list = search(flight);
//        System.out.println(list.size());
//        for(FlightModel l : list){
//            System.out.println(l.getServiceObject().getAirline().getIataCode() + " " + l.getServiceObject().flightNumber);
//        }

    }

    public static List<FlightModel> search(JSONArray x){
        List<FlightModel> flights = new ArrayList<>();
        for(int i = 0; i < x.length(); i++){
            Gson gson = new Gson();
            JSONObject j = (JSONObject) x.get(i);
            FlightModel response = gson.fromJson(String.valueOf(j), FlightModel.class);
            if(response.getServiceObjectType().equals("Flight")){
                flights.add(response);
            }
        }
        return flights;

    }
}
