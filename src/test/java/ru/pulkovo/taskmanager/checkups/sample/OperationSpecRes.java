package ru.pulkovo.taskmanager.checkups.sample;

import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;
import ru.pulkovo.taskmanager.manager.Query;
import ru.pulkovo.taskmanager.manager.TestBase;
import ru.pulkovo.taskmanager.model.OperationResourceSpecifications;

import java.util.ArrayList;
import java.util.List;

public class OperationSpecRes extends TestBase implements Query {

    public static void main(String[] args) {
        JSONObject specification = app.req().responseToString(app.req().postRequast(TEST,SPECIFICATION_RESOURCES,app.req().createDefaultVariables()).body().toString());
        JSONArray specRes = specification.getJSONObject("data").getJSONArray("operationResourceSpecifications");

        List<OperationResourceSpecifications> spec = search(specRes);
//        List<OperationResourceSpecifications.ResourceRequirements> condit = new ArrayList<>();

        for(OperationResourceSpecifications sp : spec){
            System.out.println(sp.getOperationSpecification().getName() + " " + sp.getResourceQuantityFormula());
            for(OperationResourceSpecifications.ResourceRequirements rr : sp.getResourceRequirements()){
                if(rr.getObject().getPropertyName().equals("resourceType")){
                    System.out.println(rr.getObject().getPropertyName() + " " + rr.getType() + " " + rr.getStrCon() + " " + rr.getStrVal() );
                }

            }

        }

    }



    public static List<OperationResourceSpecifications> search(JSONArray x){
        List<OperationResourceSpecifications> specRes = new ArrayList<>();

        for(int i = 0; i < x.length();i++){
            Gson gson = new Gson();
            JSONObject j = (JSONObject) x.get(i);
            OperationResourceSpecifications response = gson.fromJson(String.valueOf(j), OperationResourceSpecifications.class);
            specRes.add(response);
        }
        return specRes;
    }


}

