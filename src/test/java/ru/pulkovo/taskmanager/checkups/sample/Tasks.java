package ru.pulkovo.taskmanager.checkups.sample;

import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;
import ru.pulkovo.taskmanager.manager.Base;
import ru.pulkovo.taskmanager.manager.Query;
import ru.pulkovo.taskmanager.manager.RequastInfo;
import ru.pulkovo.taskmanager.model.Statuses;
import ru.pulkovo.taskmanager.model.TasksModel;


import java.util.*;


public class Tasks extends RequastInfo implements Query {

    public Tasks(Base app) {
        super(app);
    }

    public static void main(String[] args) {
        //response /tasks
        JSONObject res =  responseToString(postRequast(TEST,NEW_TASKS,createVariablesDay("2021","12","02")).body().toString());
        JSONArray tasks = res.getJSONObject("data").getJSONArray("tasks");

        Statuses statuses = new Statuses();

        //Create list all tasks in the date.
        List<TasksModel> task = searchTasks(tasks);

        Set<String> setFlight = new HashSet<>();
        for(TasksModel t : task){
            setFlight.add(t.getServiceObject().getServiceObject().getAirline().iataCode + " " + t.getServiceObject().getServiceObject().flightNumber);
        }

        for(int i = 0; i < setFlight.size();i++){
            Map<String,Integer> resourceList = new HashMap<>();
            for(TasksModel t : task){
                if(t.getOneFlightTasks(setFlight.stream().toList().get(i))) {
                    System.out.println(t.getInfo());
                    if(resourceList.containsKey(t.getResources().resourceType)){
                        resourceList.put(t.getResources().resourceType,resourceList.get(t.getResources().resourceType) + 1);
                    }else {
                        resourceList.put(t.getResources().resourceType, 1);
                    }
//            System.out.println(t.getOperation()[0].getOperationSpecification().getId());
                    Statuses.setArray(t.getStatus());
                }
            }
            System.out.println(resourceList + "\n");
        }



        System.out.println("Все рейсы: " + "\n" +  setFlight);
        System.out.println("Количество рейсов: (" + setFlight.size() + ")");
        System.out.println("Статусы всех задач: " + "\n" + statuses.allValueStatus());
    }

    public static List<TasksModel> searchTasks(JSONArray x){
        List<TasksModel> tasks = new ArrayList<>();
        for(int i = 0; i < x.length(); i++){
            Gson gson = new Gson();
            JSONObject j = (JSONObject) x.get(i);
            TasksModel response = gson.fromJson(String.valueOf(j), TasksModel.class);
            tasks.add(response);
        }
        return tasks;

    }


}
