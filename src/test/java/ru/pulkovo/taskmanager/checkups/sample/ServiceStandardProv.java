package ru.pulkovo.taskmanager.checkups.sample;

import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;
import ru.pulkovo.taskmanager.manager.Query;
import ru.pulkovo.taskmanager.manager.TestBase;

import java.util.ArrayList;
import java.util.List;


public class ServiceStandardProv extends TestBase implements Query {

    public static void main(String[] args) {
        JSONObject res2 = app.req().responseToString(app.req().postRequast(TEST,STANDART_PROVIDING,app.req().createDefaultVariables()).body().toString());
        JSONArray prov = res2.getJSONObject("data").getJSONArray("serviceStandardProvidings");

        //Create list all serviceStandardProviding.
        List<ru.pulkovo.taskmanager.model.ServiceStandardProv> provs = searchStandardProv(prov);
        // Список всех условий.
        List<ru.pulkovo.taskmanager.model.ServiceStandardProv.Condition> condit = new ArrayList<>();
        System.out.println(provs.size());

        for(ru.pulkovo.taskmanager.model.ServiceStandardProv p : provs){
            System.out.println("\n"+ p.getStandards().getName() + " " + "Кол-во условий: " + p.getCondition().length);
            for(ru.pulkovo.taskmanager.model.ServiceStandardProv.Condition c : p.getCondition()){
                System.out.println(c);
                System.out.println(c.getObject().getPropertyName());
                condit.add(c);
            }

        }
        System.out.println(condit);
    }
    public static List<ru.pulkovo.taskmanager.model.ServiceStandardProv> searchStandardProv(JSONArray x){
        List<ru.pulkovo.taskmanager.model.ServiceStandardProv> prov = new ArrayList<>();
        for(int i = 0; i < x.length();i++){
            Gson gson = new Gson();
            JSONObject j = (JSONObject) x.get(i);
            ru.pulkovo.taskmanager.model.ServiceStandardProv response = gson.fromJson(String.valueOf(j), ru.pulkovo.taskmanager.model.ServiceStandardProv.class);
            prov.add(response);
        }
        return prov;
    }
}
