package ru.pulkovo.taskmanager.checkups.sample;

import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;
import ru.pulkovo.taskmanager.manager.Query;
import ru.pulkovo.taskmanager.manager.TestBase;
import ru.pulkovo.taskmanager.model.FlightModel;

import java.util.ArrayList;
import java.util.List;

import static ru.pulkovo.taskmanager.manager.RequastInfo.*;


public class Flight extends TestBase implements Query {
    public static void main(String[] args) {
        JSONObject res =  responseToString(postRequast(TEST_MATH,FLIGHT,createVariablesDay("2021","11","24")).body().toString());
        JSONArray flight = res.getJSONObject("data").getJSONArray("serviceObjects");

        // без учета авиакомпаний (только рейсы) т.е (flight.ServiceObjectType == "Flight")
        List<FlightModel> flights = searchFlight(flight);

        for(FlightModel f : flights){
            System.out.println(f.getServiceObject().getAirline().getIataCode() + " " + f.getServiceObject().flightNumber);
        }
    }

    public static List<FlightModel> searchFlight(JSONArray x){
        List<FlightModel> flights = new ArrayList<>();
        for(int i = 0; i < x.length(); i++){
            Gson gson = new Gson();
            JSONObject j = (JSONObject) x.get(i);
            FlightModel response = gson.fromJson(String.valueOf(j), FlightModel.class);
            if(response.getServiceObjectType().equals("Flight")){
                flights.add(response);
            }
        }
        return flights;

    }
}
