package ru.pulkovo.taskmanager.checkups.sample;

import org.json.JSONArray;
import org.json.JSONObject;
import ru.pulkovo.taskmanager.manager.Query;
import ru.pulkovo.taskmanager.manager.TestBase;
import ru.pulkovo.taskmanager.model.Statuses;

import java.io.IOException;
import java.util.*;


public class Main extends TestBase implements Query {
    public static void main(String[] args) throws IOException {
        JSONObject res =  app.req().responseToString(app.req().postRequast(app.getProperty("api.test").toString(),LIGHT_TASKS,app.req().createVariablesDay("2021","08","18")).body().toString());
        JSONArray tasks = res.getJSONObject("data").getJSONArray("tasks");
        Statuses statuses = new Statuses();
        Set<List> set = new HashSet<>();
        Set<String> resourceTypes = new HashSet<>();


        for(int i = 0; i < tasks.length();i++){
            List<String> operat = new ArrayList();


            JSONObject j = (JSONObject) tasks.get(i);
            JSONObject service =  j.getJSONObject("serviceObject").getJSONObject("serviceObject");
            JSONObject resources = j.getJSONObject("resource");
            JSONArray operations = j.getJSONArray("operations");



            String id = (String) j.get("id");
            String status = (String) j.get("status");
            Statuses.setArray(status);
            String flightNumber = (String) service.get("flightNumber");
            String resourceType = (String) resources.get("resourceType");
            String DateStart = j.getString("scheduledStart");
            String DateEnd = j.getString("scheduledEnd");



            for(int o = 0; o < operations.length();o++){
                JSONObject x = (JSONObject) operations.get(o);
                JSONObject nm = x.getJSONObject("operationSpecification");
                String name = (String) nm.get("name");
                operat.add(name);
            }

            if(status.equals("NEW")){
                System.out.println(id + " " + status + " " + flightNumber + " " + resourceType + " " + operat +" "+ DateStart +" "+ DateEnd);
                set.add(operat);
                resourceTypes.add(resourceType);
            }




        }


        System.out.println("Операции в статусе новый: " + set);
        System.out.println("Ресурсы у которых статус новый: " + resourceTypes);
        System.out.println("Всего задач: " + tasks.length());
        System.out.println("Статусы на стенде: " + statuses.allValueStatus());


    }
}
