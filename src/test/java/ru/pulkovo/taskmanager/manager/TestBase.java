package ru.pulkovo.taskmanager.manager;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.io.IOException;

public class TestBase {
    protected static final Base app = new Base(System.getProperty("browser", "CHROME")); //BrowserType.CHROME

    @BeforeSuite (alwaysRun = true)
    public void set() throws IOException {
        app.init();
    }

    @AfterSuite (alwaysRun = true)
    public void tear(){
        app.stop();
    }


}
