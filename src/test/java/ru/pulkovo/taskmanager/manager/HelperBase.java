package ru.pulkovo.taskmanager.manager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HelperBase {

    protected Base app;
    protected WebDriver wd;

    public HelperBase(Base app){
        this.app = app;
        this.wd = app.getDriver();
    }

    protected void click(By locator){
        wd.findElement(locator).click();
    }

    protected void type(By locator, String text){
        click(locator);
        wd.findElement(locator).clear();
        wd.findElement(locator).sendKeys(text);
    }

    protected boolean check(By locator, String text){
        String checkText = wd.findElement(locator).getText();
        if(checkText.equals(text)){
            return true;
        }
        return false;
    }
}
