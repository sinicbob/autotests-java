package ru.pulkovo.taskmanager.manager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONArray;
import org.json.JSONObject;


import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;




public class RequastInfo implements Query {

    private static Base app;

    public RequastInfo(Base app) {
        this.app = app;
    }

    // Создает запрос и отдает ответ repsonse у которого есть методы .statusCode()
    // а так же .body().toString() возвращает ответ от запроса типа "Строка"
    public static HttpResponse postRequast(String url, String body, String variables) {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .setHeader("Content-Type", "application/json")
                .setHeader("Authorization","Bearer" + TOKEN )
                .POST(HttpRequest.BodyPublishers.ofString(body + variables))
                .build();
        HttpResponse response = null;
        try {
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response;
    }

    public static HttpResponse post(String url, String body) {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .setHeader("Content-Type", "application/json")
                .setHeader("Authorization","Bearer" + TOKEN )
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .build();
        HttpResponse response = null;
        try {
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response;
    }

    //парсер (Удалить нужно)
    public static JSONObject parseNorms (String responseBody){
        JSONObject obj = new JSONObject(responseBody).getJSONObject("data");
        JSONArray arr = obj.getJSONArray("operationSpecificationNorms");
        for(int i = 0; i < arr.length();i++){
            JSONArray norms = arr.getJSONObject(i).getJSONArray("conditions");
            System.out.println(norms + "\n");
        }
        return obj;
    }

    // Преобразует строку в JSONObject
    public static JSONObject responseToString(String response){
        JSONObject obj = new JSONObject(response);
        return obj;
    }

    //Создает параметры для запроса , который принемает параметры queryFrom & queryTo
    public static String createVariablesDay(String year, String month , String day){
        JSONObject y = new JSONObject();
        JSONObject x = new JSONObject();
        x.put("queryFrom", ""+year+"-"+month+"-"+day+"T00:00:00.000Z");
        x.put("queryTo", ""+year+"-"+month+"-"+day+"T23:00:00.000Z");
        y.put("filter", x);
        return y + "}";
    }
    //Создает параметры на текущий день такой же как и выше для параметров queryFrom & queryTo
    public static String createVariablesDayNow(String dateTime){
        JSONObject y = new JSONObject();
        JSONObject x = new JSONObject();
        x.put("queryFrom", ""+dateTime+"T08:00:00.000Z");
        x.put("queryTo", ""+dateTime+"T20:00:00.000Z");
        y.put("filter", x);
        return y + "}";
    }

    // Создает параметры по дефорлту типа filter:{} есть запросы которые принимают данные параметры
    public static String createDefaultVariables(){
        JSONObject y = new JSONObject();
        JSONObject x = new JSONObject();
        y.put("filter",x);
        return y + "}";
    }
    // ??? Тест (удалить нужно если не пригодится)
    public static JSONObject createJsonObject(String key, String value){
        JSONObject obj = new JSONObject();
        return obj.accumulate(key,value);
    }

    // Метод создания строки текущей даты который передается в функцию которая создается обьект для запроса
    // у которого есть параметры queryFrom & queryTo
    public static String createDateTimeNow(){
        Date date = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        String zero = "";
        String zeroday = "";
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);
        if(month < 10){
            zero = "0";
        }
        if(day < 10){
            zeroday = "0";
        }
        String dateTime = year + "-" + zero + month + "-"+ zeroday + day;
        return dateTime;
    }




}
