package ru.pulkovo.taskmanager.manager;

import com.google.gson.Gson;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;
import ru.pulkovo.taskmanager.model.FlightModel;
import ru.pulkovo.taskmanager.model.TasksModel;


import java.util.ArrayList;
import java.util.List;

import static ru.pulkovo.taskmanager.manager.RequastInfo.*;
import static ru.pulkovo.taskmanager.manager.RequastInfo.responseToString;


public class HttpHelper implements Query{
    private static Base app;
    private String url;

    public HttpHelper(Base app) {
        this.app = app;
    }

    public HttpHelper(Base app, String url ){
        this.app = app;
        if(url.equals("test")){
            this.url = TEST;
        }else if (url.equals("test-math")){
            this.url = TEST_MATH;
        }else{
            this.url = url;
        }
    }


    public JSONObject getServices(){
        return responseToString(postRequast(url,ServiceStandart,createDefaultVariables()).body().toString());
    }

    public JSONObject createService(String nameService){
        String body = "{\"input\":{\"codeRms\":\"4124412\",\"displayInProcessMap\":true,\"sortKey\":\"42151\",\"descriptionEng\":\"test\",\"descriptionRus\":\"test\",\"onDemand\":{\"calculatingOperationTimesRequired\":true,\"selectionResourcesRequired\":true},\"erpId\":\"214512\",\"name\":\""+nameService+"\",\"responsibleSubdivisionId\":\"6078d86d-f83b-4cee-bf3a-25709f451fe8\",\"type\":\"COMMERCIAL\"}}}";
        return responseToString(postRequast(url,CreateService,body).body().toString());
    }
    public JSONObject editService(String id,String name){
        String body = "{\"id\":\""+id+"\",\"input\":{\"erpId\":\"\",\"name\":\""+ name +"\",\"responsibleSubdivisionId\":\"00715d1c-3eb0-471b-b373-ad08bfe36683\",\"type\":\"COMMERCIAL\"}}}";
        return responseToString(postRequast(url,EditService,body).body().toString());
    }
    public JSONObject deleteService(String id){
        String body = "{\"id\":\""+id+"\"}}";
        return responseToString(postRequast(url,DeleteService,body).body().toString());
    }
    public JSONObject getService(String id){
        String body = "{\"id\":\""+id+"\"}}";
        return responseToString(postRequast(url,GetService,body).body().toString());
    }
    public JSONObject createOperatinoToService(String serviceId){
        String operationBody = "{\"input\":{\"arrivalPlaceExecutionTimeRequired\":true,\"departurePlaceExecutionTimeRequired\":true,\"startAndFinishOperationTimeRequired\":true,\"description\":\"test\",\"name\":\"test operation\",\"serviceStandardId\":\""+serviceId+"\",\"useSla\":true,\"code\":\"\"}}}";
        return responseToString(postRequast(url,CreateOperationToService,operationBody).body().toString());
    }
    public JSONObject createOperationSpecification(String operationSpecificationId,String countResource){
        if(countResource == null){
            countResource = "1";
        }
        String specificationBody = "{\"input\":{\"operationSpecificationId\":\""+operationSpecificationId+"\",\"resourceQuantityFormula\":\""+countResource+"\"}}}";
        return responseToString(postRequast(url,CreateOperationResourceSpecification,specificationBody).body().toString());
    }
    public JSONObject addedResourceRequirementToOperationResourceSpecification(String operationResourceSpecificationId ){
        String body = "{\"operationResourceSpecificationId\":\""+ operationResourceSpecificationId+"\",\"requirementInput\":{\"EqualityStringConditionInput\":{\"object\":{\"objectName\":\"ResourceItem\",\"propertyName\":\"resourceType\",\"propertyType\":\"String\"},\"operator\":\"eq\",\"value\":\"Employee\"}}}}";
        return responseToString(postRequast(url,AddResourceRequirementToOperationResourceSpecification,body).body().toString());
    }
    public JSONObject createServiceStandardProviding(String serviceId,Boolean onDemand){
        String standartProviding = "{\"input\":{\"onDemand\":"+onDemand+",\"serviceStandardId\":\""+serviceId+"\"}}}";
        return responseToString(postRequast(url,CreateServiceStandartProviding,standartProviding).body().toString());
    }
    public JSONObject addedConditionToServiceStandardProviding(String serviceStandardProvidingId){
        String body = "{\"serviceStandardProvidingId\":\""+serviceStandardProvidingId+"\",\"conditionInput\":{\"EqualityStringConditionInput\":{\"object\":{\"objectName\":\"Flight\",\"propertyName\":\"flightNumber\",\"propertyType\":\"String\"},\"operator\":\"eq\",\"value\":\"260996\"}}}}";
        return responseToString(postRequast(url,addConditionToServiceStandardProviding,body).body().toString());
    }
    public JSONObject createServiceOffer(String serviceId){
        String body = "{\"input\":{\"contractId\":null,\"onDemand\":false,\"serviceStandardId\":\""+ serviceId +"\",\"validFrom\":null,\"validTo\":null}}}";
        return responseToString(postRequast(url,createServiceOffer,body).body().toString());
    }

    public List<TasksModel> getTasks(String year, String month, String day){
        System.out.println("Запрос задач на: " + createVariablesDay(year,month,day) + "\n");
        JSONArray tasks = responseToString(postRequast(url,NEW_TASKS,createVariablesDay(year,month,day)).body().toString()).getJSONObject("data").getJSONArray("tasks");
        List<TasksModel> taskList = new ArrayList<>();
        for(int i = 0; i < tasks.length(); i++){
            Gson gson = new Gson();
            JSONObject j = (JSONObject) tasks.get(i);
            TasksModel response = gson.fromJson(String.valueOf(j), TasksModel.class);
            taskList.add(response);
        }
        return taskList;
    }
    public String addedFlightToMathModule(FlightModel flight){
        String body = "{\n" +
                "\t\"serviceObjects\": [ "+ new Gson().toJson(flight, FlightModel.class) +" ]\n" +
                "}";
        return post("http://operational-planning-test-math.apps.okd.mrms.pulkovo-airport.com/backend-math/api/v1/calculate_custom_service_objects",body).body().toString();
    }


}
