package ru.pulkovo.taskmanager.manager;

import org.openqa.selenium.By;

import java.io.IOException;

public class SessionHelper extends HelperBase{

    public SessionHelper(Base app){
        super(app);
        wd = app.getDriver();
    }

    public void login(String username, String password) throws IOException {
        wd.get(app.getProperty("test"));
        type(By.xpath("//*[@id=\"app\"]/div/form/div[1]/div/input"),username);
        type(By.xpath("//*[@id=\"app\"]/div/form/div[2]/div/input"),password);
        click(By.xpath("//*[@id=\"app\"]/div/form/div[3]/button"));
    }
    public boolean passlogin(){
        return check(By.xpath("//*[@class=\"menu-label\"]"),"МЕНЮ");
    }
    public boolean failLogin(){
        return check(By.xpath("//*[@id=\"app\"]/div/form/div[3]/p"),"Full authentication is required to access this resource");
    }

    public void logout(){
        click(By.xpath("//span[@class='icon-text']"));
        click(By.xpath("//*[@id=\"app\"]/div[2]/div[2]/footer/button[2]"));
    }
    public boolean passLogout(){
        return check(By.xpath("//*[@id=\"app\"]/div/form/div[3]/button"),"Войти");
    }
}
