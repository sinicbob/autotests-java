package ru.pulkovo.taskmanager.manager;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Base {
    private final Properties properties;
    public WebDriver wd;
    private String browser;
    private RequastInfo req;
    private UIHelper ui;
    private SessionHelper sessionHelper;
    private HttpHelper request;
    private DbHelper db;

    public Base(String browser) {
        this.browser = browser;
        properties = new Properties();
    }

    public void init() throws IOException {
        String target = System.getProperty("target","local");
        properties.load(new FileReader(new File(String.format("src/test/resources/%s.properties",target))));
    }

    public void stop() {
        if(wd != null){
            wd.quit();
        }
    }

    public String getProperty(String key) throws IOException {
        properties.load(new FileReader(new File("src/test/resources/local.properties")));
        return properties.getProperty(key);
    }

    public WebDriver getDriver(){
        if(wd == null){
            wd = new ChromeDriver();
            wd.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            wd.get(properties.getProperty("test"));
        }
        return wd;
    }

    public RequastInfo req(){
        if(req == null){
            req = new RequastInfo(this);
        }
        return req;
    }

    public DbHelper db(){
        if(db == null){
            db = new DbHelper(this);
        }
        return db;
    }

    public UIHelper ui(){
        if(ui == null){
            ui = new UIHelper(this);
        }
        return ui;
    }
    public SessionHelper session(){
        if(sessionHelper == null){
            sessionHelper = new SessionHelper(this);
        }
        return sessionHelper;
    }
    public HttpHelper request(String url){
        if(request == null){
            request = new HttpHelper(this,url);
        }
        return request;
    }

}
