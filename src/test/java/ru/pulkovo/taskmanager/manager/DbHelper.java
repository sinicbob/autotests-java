package ru.pulkovo.taskmanager.manager;

import java.sql.*;
import java.util.Properties;

public class DbHelper {

    protected Base app;
    public Connection connection;

    public DbHelper(Base app) {
        this.app = app;

        String url = "jdbc:postgresql://pg.test-math.mrms.pulkovo-airport.com:5432/taskmanagerdb";
        Properties props = new Properties();
        props.setProperty("user","taskmanager");
        props.setProperty("password","p5PnKxy9MWjhNVzZ");
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            this.connection = DriverManager.getConnection(url, props);
            System.out.println("Подключение к бд успешно");
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {

        }
    }


    public void test() throws SQLException {
        Statement st = this.connection.createStatement();
        ResultSet rs = st.executeQuery("select * from public.flight where flight_number = '7005' ");
        while (rs.next())
        {
            System.out.print("Column 1 returned ");
            System.out.println(rs.getString(1));
        }
        rs.close();
        st.close();
        this.connection.close();
    }
    public void createFlightDB(String values) throws SQLException {
        Statement st = this.connection.createStatement();
        String text = "INSERT INTO public.flight (id,flight_number,aircraft_id,aircraft_type_id,airline_id,cancellation_datetime,parking_spot_id,parking_spot_scheduled_start_datetime,parking_spot_scheduled_end_datetime,traffic_type,linked_flight_id,aodb_internal_id,num_id,arrival_airport_id,departure_airport_id,gate_id,special_emphasis_id,flight_service_type_id,aidx_status,aodb_internal_status,delay_time,crew_number,cabin_crew_number,last_update_date_time,deleted_at,aircraft_terminal,linked_flight_num_id,invoice_id,ready_for_billing,payment_method,origin_date,created_at,updated_at) VALUES";
         int result = st.executeUpdate(text + " " + values);
        System.out.println("Рейс добавлен в бд" + " " + result);
        st.close();
        this.connection.close();
    }

}
