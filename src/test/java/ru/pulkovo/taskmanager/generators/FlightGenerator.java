package ru.pulkovo.taskmanager.generators;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ru.pulkovo.taskmanager.model.FlightModel;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class FlightGenerator {

    public int count = 1;

    public static void main(String[] args) throws IOException {
        FlightGenerator generator = new FlightGenerator();
        generator.run();
    }

    private void run() throws IOException {
        List<FlightModel> flights = generateFlight(count);
        saveAsJSON(flights,new File("src/test/resources/flights.json"));
    }

    public void saveAsJSON(List<FlightModel> flight, File file) throws IOException {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(flight);
        try(Writer writer = new FileWriter(file);){
            writer.write(json);
        }
    }

    public FlightModel generateOneFlight() {
        return new FlightModel();
    }

    private List<FlightModel> generateFlight(int count) {
        List<FlightModel> flights = new ArrayList<>();
        for(int i = 0; i < count;i++){
            FlightModel flight = new FlightModel();
            flights.add(flight);
        }
        return flights;
    }
}
