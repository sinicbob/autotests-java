package ru.pulkovo.taskmanager.diff_json;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import groovy.json.JsonToken;
import org.joda.time.DateTime;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.pulkovo.taskmanager.manager.TestBase;
import ru.pulkovo.taskmanager.model.TasksModel;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;



public class DiffJson extends TestBase {

    @BeforeMethod
    public void downloadJsonTasks() throws IOException {
        System.out.println("Дата и время запуска " + DateTime.now());
        if(!new File("src/test/resources/tasks.json").exists()){
            List<TasksModel> tasks = app.request("test").getTasks("2021","12","03");
            SaveJsonFile(tasks);
            System.out.println("Создан файл с задачами");
        }
    }

    @Test
    public void diffJsonTasks() throws IOException {
        //Взять данные из файла
        List<TasksModel> tasksInFile = read();
//        for(TasksModel t : tasksInFile){
//            System.out.println(t.getInfo());
//        }
        //Взять данные с нового запроса.
        List<TasksModel> tasks = app.request("test").getTasks("2021","12","03");
        List<TasksModel> newTask = new ArrayList<>();
        //Сравнить данные из файла с данными из запроса
        for(TasksModel t: tasks){
            for(TasksModel t2: tasksInFile){
                if(t.getId().equals(t2.getId())){
                    if(!t.getStatus().equals(t2.getStatus())){
                        System.out.println(t.getInfo() + " (Changed status) " + " Old status " + t.getStatus() + " New status " + t2.getStatus() );
                    }
                    if(!t.getScheduledStart().equals(t2.getScheduledStart())){
                        System.out.println(t.getInfo() + " (Changed ScheduledStart) ");
                    }
                }
            }
        }
        //Выдать результат
        System.out.println("File tasks:" + tasksInFile.size() + " TasksInResponse: " + tasks.size());
        //Сохранить новое тело запроса в файл.
        SaveJsonFile(tasks);
    }

    private List<TasksModel>  read() {
        List<TasksModel> oldTasks = null;
        try {
            Reader reader = Files.newBufferedReader(Paths.get("src/test/resources/tasks.json"));
            oldTasks = new Gson().fromJson(reader, new TypeToken<List<TasksModel>>() {}.getType());
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JsonIOException e) {
            e.printStackTrace();
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return oldTasks;
    }


    private void SaveJsonFile(List<TasksModel> tasks) throws IOException {
        Gson gson = new GsonBuilder().setPrettyPrinting().enableComplexMapKeySerialization().create();
        String json = gson.toJson(tasks);
        try(Writer writer = new FileWriter("src/test/resources/tasks.json")){
            writer.write(json);
        }
    }
}
