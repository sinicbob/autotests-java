package ru.pulkovo.taskmanager.tests.services;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.pulkovo.taskmanager.manager.TestBase;

public class EditService extends TestBase {
    @Test
    public void editService(){
        JSONArray services = app.request("test").getServices().getJSONObject("data").getJSONArray("serviceStandards");
        String serviceId = "";
        String name = "";
        for(int i = 0; i < services.length();i++){
            if(services.getJSONObject(i).get("name").equals("new Test")){
                serviceId = (String) services.getJSONObject(i).get("id");
                name = (String) services.getJSONObject(i).get("name");
            }
        }
        String newName = "edit-test";
        app.request("test").editService(serviceId,newName);
        Assert.assertNotEquals(name, newName);
        System.out.println("Сервис изменен " + serviceId);
    }
}
