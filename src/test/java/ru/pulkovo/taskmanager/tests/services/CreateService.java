package ru.pulkovo.taskmanager.tests.services;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.pulkovo.taskmanager.manager.TestBase;

public class CreateService extends TestBase {

    @Test
    public void getService(){
        String serviceId = (String) app.request("test").createService("new Test").getJSONObject("data").getJSONObject("createServiceStandard").get("id");
        String name = (String) app.request("test").getService(serviceId).getJSONObject("data").getJSONObject("serviceStandard").get("name");
        Assert.assertEquals(name, "new Test");
        System.out.println("Создан сервис " + serviceId);
    }
}
