package ru.pulkovo.taskmanager.tests.services;

import org.json.JSONArray;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.pulkovo.taskmanager.manager.TestBase;

public class DeleteService extends TestBase {

    @Test
    public void deleteService(){
        JSONArray services = app.request("test").getServices().getJSONObject("data").getJSONArray("serviceStandards");
        String serviceId = "";
        for(int i = 0; i < services.length();i++){
            if(services.getJSONObject(i).get("name").equals("edit-test")){
                serviceId = (String) services.getJSONObject(i).get("id");
            }
        }
        app.request("test").deleteService(serviceId);
        JSONArray after = app.request("test").getServices().getJSONObject("data").getJSONArray("serviceStandards");
        Assert.assertEquals(after.length(),services.length() -1);
        System.out.println("Сервис удален " + serviceId);
    }
}
