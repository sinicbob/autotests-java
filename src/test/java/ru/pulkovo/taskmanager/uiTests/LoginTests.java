package ru.pulkovo.taskmanager.uiTests;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.pulkovo.taskmanager.manager.TestBase;

import java.io.IOException;

public class LoginTests extends TestBase {


    @Test
    public void testBadLogin() throws IOException, InterruptedException {
        app.session().login("gasgasg","41251sf");
        Thread.sleep(1000);
        Assert.assertTrue(app.session().failLogin());
    }

    @Test
    public void testLogin() throws IOException, InterruptedException {
        app.session().login(app.getProperty("test_login"),app.getProperty("test_password"));
        Thread.sleep(1000);
        Assert.assertTrue(app.session().passlogin());
    }

    @Test
    public void testLogout() throws InterruptedException {
        app.session().logout();
        Thread.sleep(1000);
        Assert.assertTrue(app.session().passLogout());
    }

}
