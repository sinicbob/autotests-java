package ru.pulkovo.taskmanager.requestsTests;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.pulkovo.taskmanager.manager.Query;
import ru.pulkovo.taskmanager.manager.TestBase;

import java.net.http.HttpResponse;

import static ru.pulkovo.taskmanager.manager.RequastInfo.*;


public class ServiceStandartProviding extends TestBase implements Query {

    @Test
    public void providingStatusCode(){
        HttpResponse providing = app.req().postRequast(TEST,STANDART_PROVIDING,app.req().createDefaultVariables());
        int providingCode = providing.statusCode();
        Assert.assertEquals(providingCode,200);
    }

    @Test
    public void providingResponseNotNull(){
        HttpResponse providing = app.req().postRequast(TEST,STANDART_PROVIDING,app.req().createDefaultVariables());
        JSONObject prov = app.req().responseToString(providing.body().toString());
        JSONArray pr = prov.getJSONObject("data").getJSONArray("serviceStandardProvidings");
        Assert.assertTrue(pr.length() > 0);
    }

    @Test
    public void providingDataErrors(){
        HttpResponse providing = app.req().postRequast(TEST,STANDART_PROVIDING,app.req().createDefaultVariables());
        JSONObject prov = app.req().responseToString(providing.body().toString());
        JSONObject pr = prov.getJSONObject("data");
        Assert.assertNotNull(pr);
    }

}
