package ru.pulkovo.taskmanager.requestsTests;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.pulkovo.taskmanager.manager.Query;
import ru.pulkovo.taskmanager.manager.TestBase;

import java.net.http.HttpResponse;

import static ru.pulkovo.taskmanager.manager.RequastInfo.*;


public class ResourcesTests extends TestBase implements Query {

    @Test
    public void resourcesStatusCode(){
        HttpResponse resources = app.req().postRequast(TEST,RESOURCES,app.req().createDefaultVariables());
        int resourcesCode = resources.statusCode();
        Assert.assertEquals(resourcesCode,200);
    }

    @Test
    public void resourcesResponseNotNull(){
        HttpResponse resources = app.req().postRequast(TEST,RESOURCES,app.req().createDefaultVariables());
        JSONObject res = app.req().responseToString(resources.body().toString());
        JSONArray rs = res.getJSONObject("data").getJSONArray("resources");
        Assert.assertTrue(rs.length() > 0);
    }
    @Test
    public void resourcesDataErrors(){
        HttpResponse resources = app.req().postRequast(TEST,RESOURCES,app.req().createDefaultVariables());
        JSONObject res = app.req().responseToString(resources.body().toString());
        JSONObject rs = res.getJSONObject("data");
        Assert.assertNotNull(rs);
    }
}
