package ru.pulkovo.taskmanager.requestsTests;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.pulkovo.taskmanager.manager.Query;
import ru.pulkovo.taskmanager.manager.TestBase;

import java.net.http.HttpResponse;

import static ru.pulkovo.taskmanager.manager.RequastInfo.*;


public class TasksTests extends TestBase implements Query {
    private HttpResponse tasks = postRequast(TEST,NEW_TASKS,createVariablesDayNow(createDateTimeNow()));
    private int taskCode = tasks.statusCode();
    private JSONObject task = responseToString(tasks.body().toString());
    private JSONArray ts = task.getJSONObject("data").getJSONArray("tasks");

    @Test
    public void tasksStatusTests(){
        Assert.assertEquals(taskCode,200);
    }

    @Test
    public void tasksResponseNotNull(){
        Assert.assertTrue(ts.length() > 0);
    }

    @Test
    public void tasksDataErrors(){
        Assert.assertNotNull(ts);
    }

}
