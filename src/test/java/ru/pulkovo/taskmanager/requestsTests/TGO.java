package ru.pulkovo.taskmanager.requestsTests;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.pulkovo.taskmanager.manager.Query;
import ru.pulkovo.taskmanager.manager.TestBase;

import java.net.http.HttpResponse;

import static ru.pulkovo.taskmanager.manager.RequastInfo.postRequast;
import static ru.pulkovo.taskmanager.manager.RequastInfo.responseToString;


public class TGO extends TestBase implements Query {

    @Test
    public void tgoStatusCode(){
        HttpResponse tgoRes = postRequast(TEST,TGO,"{}}");
        int tgoCode = tgoRes.statusCode();
        Assert.assertEquals(tgoCode,200);
    }

    @Test
    public void tgoResponseNotNull(){
        HttpResponse tgoRes = app.req().postRequast(TEST,TGO,"{}}");
        JSONObject tgo = app.req().responseToString(tgoRes.body().toString());
        JSONArray tg = tgo.getJSONObject("data").getJSONArray("operationSpecificationTimeCalculationRules");
        Assert.assertTrue(tg.length() > 0);
    }

    @Test
    public void tgoDataErrors(){
        HttpResponse tgoRes = app.req().postRequast(TEST,TGO,"{}}");
        JSONObject tgo = app.req().responseToString(tgoRes.body().toString());
        JSONArray tg = tgo.getJSONObject("data").getJSONArray("operationSpecificationTimeCalculationRules");
        Assert.assertNotNull(tg);
    }
}
