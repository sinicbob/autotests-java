package ru.pulkovo.taskmanager.requestsTests;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.pulkovo.taskmanager.manager.Query;
import ru.pulkovo.taskmanager.manager.TestBase;

import java.net.http.HttpResponse;

import static ru.pulkovo.taskmanager.manager.RequastInfo.*;


public class SpecificationResource extends TestBase implements Query {

    @Test
    public void specificationResStatusCode(){
        HttpResponse specificationRes = app.req().postRequast(TEST,SPECIFICATION_RESOURCES,app.req().createDefaultVariables());
        int specificationResCode = specificationRes.statusCode();
        Assert.assertEquals(specificationResCode,200);
    }

    @Test
    public void specificationResResponseNotNull(){
        HttpResponse specificationRes = app.req().postRequast(TEST,SPECIFICATION_RESOURCES,app.req().createDefaultVariables());
        JSONObject specification = app.req().responseToString(specificationRes.body().toString());
        JSONArray sp = specification.getJSONObject("data").getJSONArray("operationResourceSpecifications");
        Assert.assertTrue(sp.length() > 0);
    }

    @Test
    public void specificationResDataErrors(){
        HttpResponse specificationRes = app.req().postRequast(TEST,SPECIFICATION_RESOURCES,app.req().createDefaultVariables());
        JSONObject specification = app.req().responseToString(specificationRes.body().toString());
        JSONObject sp = specification.getJSONObject("data");
        Assert.assertNotNull(sp);
    }
}
