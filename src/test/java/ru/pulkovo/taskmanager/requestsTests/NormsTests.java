package ru.pulkovo.taskmanager.requestsTests;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.pulkovo.taskmanager.manager.Query;
import ru.pulkovo.taskmanager.manager.TestBase;

import java.net.http.HttpResponse;

import static ru.pulkovo.taskmanager.manager.RequastInfo.*;


public class NormsTests extends TestBase implements Query {
    private HttpResponse norms = app.req().postRequast(TEST,NORMS,app.req().createDefaultVariables());
    private int normCode = norms.statusCode();
    private JSONObject norm = app.req().responseToString(norms.body().toString());
    private JSONArray nr = norm.getJSONObject("data").getJSONArray("operationSpecificationNorms");

    @Test
    public void normsStatusCode(){
        Assert.assertEquals(normCode,200);
    }

    @Test
    public void normsResponseNotNull(){
        Assert.assertTrue(nr.length() > 0);
    }

    @Test
    public void normDataErrors(){
        Assert.assertNotNull(nr);
    }
}
