package ru.pulkovo.taskmanager.requestsTests;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.pulkovo.taskmanager.manager.Query;
import ru.pulkovo.taskmanager.manager.TestBase;

import java.io.IOException;
import java.net.http.HttpResponse;

import static ru.pulkovo.taskmanager.manager.RequastInfo.*;


public class FlightTests extends TestBase implements Query {

    private HttpResponse flight;
    private int flightCode ;
    private JSONObject fly;
    private JSONArray fl;

    @BeforeClass
    public void setUp() throws IOException {
        flight = postRequast(TEST,FLIGHT,createVariablesDayNow(createDateTimeNow()));
        flightCode = flight.statusCode();;
        fly = responseToString(flight.body().toString());;
        fl = fly.getJSONObject("data").getJSONArray("serviceObjects");;
    }

    // Тест проверки на статус 200
    @Test
    public void flightStatusCode(){
        Assert.assertEquals(flightCode,200);
    }

    // Тест проверки что ответ содержит не пустой массив ![]
    @Test
    public void flightResponseNotNull(){
        Assert.assertTrue(fl.length() > 0);
    }

    // Тест проверки то что в ответе нет обьекта data null , при котором отображается обьект error(s) в котором указываются
    // в котором указываются ошибки запроса (прочие ошибки)
    @Test
    public void normDataErrors(){
        Assert.assertNotNull(fl);
    }

}
