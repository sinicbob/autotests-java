package ru.pulkovo.taskmanager.model;

public class OperationResourceSpecifications {
    private OperationSpecification operationSpecification;
    private String resourceQuantityFormula;
    private ResourceRequirements[] resourceRequirements;

    public ResourceRequirements[] getResourceRequirements() {
        return resourceRequirements;
    }

    public String getResourceQuantityFormula() {
        return resourceQuantityFormula;
    }

    public OperationSpecification getOperationSpecification() {
        return operationSpecification;
    }

    public static class OperationSpecification{
        private String id;
        private String name;

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }
    public static class ResourceRequirements{
        private String id;
        private ServiceStandardProv.Obj object;
        private String type;
        private String strCon;
        private String strVal;

        public String getStrCon() {
            return strCon;
        }

        public String getStrVal() {
            return strVal;
        }

        public String getId() {
            return id;
        }

        public ServiceStandardProv.Obj getObject() {
            return object;
        }

        public String getType() {
            return type;
        }
    }

}

