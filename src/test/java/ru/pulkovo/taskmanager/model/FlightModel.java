package ru.pulkovo.taskmanager.model;

import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.poi.sl.draw.binding.STTextShapeType;
import org.checkerframework.checker.units.qual.A;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.Date;
import java.util.UUID;

public class FlightModel {

    @SerializedName("serviceObject")
    private ServiceObjects serviceObject;
    @SerializedName("serviceObjectType")
    private String serviceObjectType;

    public FlightModel(FlightModel response) {
        this.serviceObject = response.getServiceObject();
        this.serviceObjectType = response.getServiceObjectType();
    }

    public FlightModel() {
        serviceObject = new ServiceObjects();
        serviceObjectType = "Flight";
    }


    // serviceObject
    public static class ServiceObjects{
       public FlightSpecifications flightSpecification;
       public AircraftType aircraftType;
       @SerializedName("airline")
       public Airlines airline;
       public String flightNumber;
       public String id;
       public String estimatedArrivalDatetime;
       public String  scheduledArrivalDatetime;
       public String estimatedDepartureDatetime;
       public String scheduledDepartureDatetime;
       public String readyForBilling;
       public String aodbInternalStatus;
//       public String estimatedDepartureDatetime;
//        aircraft
       @SerializedName("parkingSpot")
       public ParkingSpots parkingSpot;
       @SerializedName("linkedFlight")
       public LinkedFlights linkedFlight;
       @SerializedName("serviceType")
       public ServiceTypes serviceType;
       @SerializedName("trafficType")
       public String trafficType;

       public ServiceObjects(){
           this.id = String.valueOf(UUID.randomUUID());
           this.flightSpecification = new FlightSpecifications();
           this.aircraftType = new AircraftType();
           this.airline = new Airlines();
           this.parkingSpot = new ParkingSpots();
           this.linkedFlight = new LinkedFlights();
           this.serviceType = new ServiceTypes();
           this.flightNumber = "null";
           this.estimatedArrivalDatetime = null;
           this.scheduledArrivalDatetime = null;
           this.estimatedDepartureDatetime = null;
           this.scheduledDepartureDatetime = null;
           this.readyForBilling = "BIL";
           this.aodbInternalStatus = "280";
           this.trafficType = "I";
       }



        public void setArrivalDatetime(DateTime estimatedArrivalDatetime) {
           if(estimatedArrivalDatetime == null){
               estimatedArrivalDatetime = new DateTime().toDateTime(DateTimeZone.UTC);
           }
           this.estimatedArrivalDatetime = String.valueOf(estimatedArrivalDatetime.toDateTime(DateTimeZone.UTC));
           this.scheduledArrivalDatetime = String.valueOf(estimatedArrivalDatetime.toDateTime(DateTimeZone.UTC));
        }

        public void setDepartureDatetime(DateTime estimatedDepartureDatetime) {
            if(estimatedDepartureDatetime == null){
                estimatedDepartureDatetime = new DateTime().toDateTime(DateTimeZone.UTC);
            }
            this.estimatedDepartureDatetime = String.valueOf(estimatedDepartureDatetime.toDateTime(DateTimeZone.UTC));
            this.scheduledDepartureDatetime = String.valueOf(estimatedDepartureDatetime.toDateTime(DateTimeZone.UTC));
        }

        public FlightSpecifications getFlightSpecification() {
           return flightSpecification;
       }

       public AircraftType getAircraftType() {
           return aircraftType;
       }

       public Airlines getAirline() {
           if(airline == null){
               return airline = new Airlines();
           }else {
               return airline;
           }
       }

       public ParkingSpots getParkingSpot() {
           if(parkingSpot == null){
               return parkingSpot = new ParkingSpots();
           }else {
               return parkingSpot;
           }
       }

        public ServiceObjects setFlightNumber(String flightNumber) {
            this.flightNumber = flightNumber;
            return this;
        }

        public ServiceTypes getServiceType() {
            return serviceType;
        }

        public String getFlightNumber() {
            return flightNumber;
        }

        public String getId() {
            return id;
        }

        public String getTrafficType() {
            return trafficType;
        }

        public void setTrafficType(String trafficType) {
            this.trafficType = trafficType;
        }

        public void setAircraftType(AircraftType aircraftType) {
            this.aircraftType = aircraftType;
        }

        public void setAirline(Airlines airline) {
            this.airline = airline;
        }


        @Override
        public String toString() {
            return "ServiceObjects{" +
                    "flightSpecification=" + flightSpecification +
                    ", aircraftType=" + aircraftType +
                    ", airline=" + airline +
                    ", flightNumber='" + flightNumber + '\'' +
                    ", id='" + id + '\'' +
                    ", parkingSpot=" + parkingSpot +
                    ", linkedFlight=" + linkedFlight +
                    ", serviceType=" + serviceType +
                    ", trafficType='" + trafficType + '\'' +
                    '}';
        }
        public String toStringBD() {
            return "(" +
                    "\'" + id + "\'" +
                    ",\'" + flightNumber + "\'" +
                    ",\'" + null + "\'" +
                    ",\'" + null + "\'" +
                    ",\'" + null + "\'" +
                    "," + null + "" +
                    ",\'" + null + "\'" +
                    "," + null + "" +
                    "," + null + "" +
                    ",\'" + trafficType + "\'" +
                    ",\'" + null + "\'" +
                    ",\'" + null + "\'" +

                    "," + 0 + "" +
                    ",\'" + null + "\'" +
                    ",\'" + null + "\'" +
                    ",\'" + null + "\'" +
                    ",\'" + null + "\'" +
                    ",\'" + null + "\'" +
                    ",\'" + null + "\'" +
                    ",\'" + null + "\'" +
                    ",\'" + null + "\'" +
                    "," + 0 + "" +

                    "," + 0 + "" +
                    "," + null + "" +
                    "," + null + "" +
                    ",\'" + null + "\'" +
                    ",\'" + null + "\'" +
                    "," + null + "" +
                    ",\'" + readyForBilling + "\'" +
                    ",\'" + "C" + "\'" +

                    ",\'" + new DateTime().toLocalDate() + " " +  new DateTime().toLocalTime() + "\'" +
                    ",\'" + new DateTime().toLocalDate() + " " +  new DateTime().toLocalTime() + "\'" +
                    ",\'" + new DateTime().toLocalDate() + " " +  new DateTime().toLocalTime() + "\'" +
                    ')';
        }
    }

   // airline
   public static class Airlines {
       public String iataCode;
       public String icaoCode;
       public String id;
       public String name;
       //registredIn

       public Airlines(){
           this.iataCode = "null";
           this.icaoCode = "null";
           this.id = "null";
           this.name = "null";

       }

       public String getIataCode() {
           return iataCode;
       }

       @Override
       public String toString() {
           return "Airlines{" +
                   "iataCode='" + iataCode + '\'' +
                   ", icaoCode='" + icaoCode + '\'' +
                   ", id='" + id + '\'' +
                   ", name='" + name + '\'' +
                   '}';
       }
   }

   // flightSpecification
   public static class FlightSpecifications {
       @SerializedName("arrivalAirport")
       public ArrivalAndDeparture arrivalAirport;
       @SerializedName("departureAirport")
       public ArrivalAndDeparture departureAirport;

        public ArrivalAndDeparture getDepartureAirport() {
            return departureAirport;
        }
        public ArrivalAndDeparture getArrivalAirport() {
            return arrivalAirport;
        }

        public FlightSpecifications(){
            this.arrivalAirport = new ArrivalAndDeparture();
            this.departureAirport = new ArrivalAndDeparture();
        }
        

        @Override
        public String toString() {
            return "FlightSpecifications{" +
                    "arrivalAirport=" + arrivalAirport +
                    ", departureAirport=" + departureAirport +
                    '}';
        }
    }

    // flightSpecification.arrivalAirport & flightSpecification.departureAirport
    public static class ArrivalAndDeparture {
       public String id;
       public String iataCode;

        public String getIataCode() {
            return iataCode;
        }
        public String getId() {
            return id;
        }

        public ArrivalAndDeparture(){
            this.id = "null";
            this.iataCode = "null";
        }

        @Override
        public String toString() {
            return "ArrivalAndDeparture{" +
                    "id='" + id + '\'' +
                    ", iataCode='" + iataCode + '\'' +
                    '}';
        }

    }

    // aircraftType
    public static class AircraftType {
        public String height;
        public String iataCode;
        public String icaoCode;
        public String id;
        public String length;
        public String name;

        public AircraftType(){
            this.height = "null";
            this.iataCode = "null";
            this.icaoCode = "null";
            this.id = "null";
            this.length = "null";
            this.name = "null";

        }

        public String getIataCode() {
            return iataCode;
        }

        @Override
        public String toString() {
            return "AircraftType{" +
                    "height='" + height + '\'' +
                    ", iataCode='" + iataCode + '\'' +
                    ", icaoCode='" + icaoCode + '\'' +
                    ", id='" + id + '\'' +
                    ", length='" + length + '\'' +
                    ", name='" + name + '\'' +
                    '}';
        }
    }

    // parkingSpot
    public static class ParkingSpots {
        public String number;
        public String platform;
        public String type;

        public ParkingSpots(){
            this.number = "null";
            this.platform = "null";
            this.type = "null";
        }

        public String getNumber() {
                return number;
        }

        public String getPlatform() {
            return platform;
        }

        public String getType() {
            return type;
        }

        @Override
        public String toString() {
            return "ParkingSpots{" +
                    "number='" + number + '\'' +
                    ", platform='" + platform + '\'' +
                    ", type='" + type + '\'' +
                    '}';
        }
    }

    public static class LinkedFlights {
       public String trafficType;

       public LinkedFlights(){
           this.trafficType = "null";
       }

        public String getTrafficType() {
            return trafficType;
        }

        @Override
        public String toString() {
            return "linkedFlight{" +
                    "trafficType='" + trafficType + '\'' +
                    '}';
        }
    }

    public static class ServiceTypes {
       public String code;

       public ServiceTypes(){

       }

        public String getCode() {
            return code;
        }

        @Override
        public String toString() {
            return "ServiceTypes{" +
                    "code='" + code + '\'' +
                    '}';
        }
    }


    public ServiceObjects getServiceObject() {
        return serviceObject;
    }

    public String getServiceObjectType() {
        return serviceObjectType;
    }

    @Override
    public String toString() {
        return "FlightModel{" +
                "serviceObject=" + serviceObject +
                ", serviceObjectType='" + serviceObjectType + '\'' +
                '}';
    }


}