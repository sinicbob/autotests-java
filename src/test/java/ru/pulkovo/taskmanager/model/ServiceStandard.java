package ru.pulkovo.taskmanager.model;

public class ServiceStandard {
    private String id;
    private String name;

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "ServiceStandard{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
