package ru.pulkovo.taskmanager.model;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;
import java.util.List;

public class TasksModel {

    private String id;
    private String status;

    private String scheduledStart;
    @SerializedName("serviceObject")
    private Service serviceObject;
    @SerializedName("resource")
    private Resources resources;
    @SerializedName("operations")
    private Operations[] operation;

    private List<OperationResourceSlots> operationResourceSlots;

    private List<Operations> listOper = operationList(getOperation());


    public TasksModel(String id, String status, String  serviceObject, String resources) {
        this.id = id;
        this.status = status;
        if (this.getServiceObject() != null) {
            this.getServiceObject().getServiceObject().flightNumber = serviceObject;
        }
        if (this.getResources() != null) {
            this.getResources().resourceType = resources;
        }
    }

    public String getScheduledStart() {
        return scheduledStart;
    }

    public class Service  {
        @SerializedName("serviceObject")
        private FlightModel.ServiceObjects serviceObject;
        @SerializedName("serviceObjectType")
        private String serviceObjectType;

        public FlightModel.ServiceObjects getServiceObject() {
            return serviceObject;
        }

        public String getServiceObjectType() {
            return serviceObjectType;
        }

        @Override
        public String toString() {
            return "ServiceObjects{" +
                    "serviceObject=" + serviceObject +
                    ", serviceObjectType='" + serviceObjectType + '\'' +
                    '}';
        }
    }

    public static class Resources {
        @SerializedName("resource")
        public Resource resource;
        public String resourceType;


        public String getResourceType() {
            return resourceType;
        }
        public boolean getResourceTypeEq(String type){
            if(resourceType.equals(type)){
                return true;
            }else {
                return false;
            }
        }

        public Resource getResource() {
            return resource;
        }
    }

    public static class Resource {
        public String id;

        public String getId() {
            return id;
        }
    }

    public static class Operations {
        public String id;
        public OperationSpecification operationSpecification;

        public String getId() {
            return id;
        }

        public OperationSpecification getOperationSpecification() {
            return operationSpecification;
        }

        @Override
        public String toString() {
            return "Operations{" +
                    "id='" + id + '\'' +
                    ", operationSpecification=" + operationSpecification +
                    '}';
        }
    }
    public static class OperationResourceSlots{
        private String id;
        private String status;
        private List<OperationResourceRequirements> operationResourceRequirements;

        public String getId() {
            return id;
        }

        public String getStatus() {
            return status;
        }

        public List<OperationResourceRequirements> getOperationResourceRequirements() {
            return operationResourceRequirements;
        }

        @Override
        public String toString() {
            return "OperationResourceSlots{" +
                    "id='" + id + '\'' +
                    ", status='" + status + '\'' +
                    ", operationResourceRequirements=" + operationResourceRequirements +
                    '}';
        }
    }

    public static class OperationResourceRequirements<T>{
        private String id;
        private Obj object;
        private String type;
        @SerializedName(value = "strCon", alternate={"iSCon", "fcOp", "icOp", "conConOp" , "boolCon" , "flCon" , "eqCon", "allConOp", "allOfConOp" , "hasOneOp" , "hasOneStrCon", "iiCon", "emptyOp"} ) // alternate={"iSCon", "fcOp", "icOp", "conConOp" , "boolCon" , "flCon" , "eqCon", "allConOp", "allOfConOp" , "hasOneOp" , "hasOneStrCon", "iiCon", "emptyOp"}
        public String condition;
        @SerializedName(value = "strVal",alternate={"iSVal","fcVal", "icVal" , "conConVal", "boolVal" , "flVal" , "eqVal", "allValOp" , "allOfConVal" , "hasOneVal", "hasOneStrVal" , "iiVal"} ) //  alternate={"iSVal","fcVal", "icVal" , "conConVal", "boolVal" , "flVal" , "eqVal", "allValOp" , "allOfConVal" , "hasOneVal", "hasOneStrVal" , "iiVal"}
        public T value;

        public Obj getObject() {
            return object;
        }

        public String getId() {
            return id;
        }

        public String getCondition() {
            return condition;
        }

        public T getValue() {
            return value;
        }

        @Override
        public String toString() {
            return "OperationResourceRequirements{" +
                    "id='" + id + '\'' +
                    ", object=" + object +
                    ", type='" + type + '\'' +
                    ", condition='" + condition + '\'' +
                    ", value='" + value + '\'' +
                    '}';
        }
    }

    public static class Obj{
        private String objectName;
        private String propertyName;
        private String propertyType;

        public String getPropertyName() {
            return propertyName;
        }

        @Override
        public String toString() {
            return "Obj{" +
                    "objectName='" + objectName + '\'' +
                    ", propertyName='" + propertyName + '\'' +
                    ", propertyType='" + propertyType + '\'' +
                    '}';
        }
    }

    public static class OperationSpecification{
        public String id;
        public String name;

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }
    //Получение id задачи
    public String getId() {
        return id;
    }
    //Получение объекта рейс из задачи
    public Service getServiceObject() {
        return serviceObject;
    }
    //Получение поле статус
    public String getStatus() {
        return status;
    }
    public List<OperationResourceSlots> getOperationResourceSlots() {
        return operationResourceSlots;
    }
    // получение задач с одним и тем же статусом
    public boolean getStatuses(String status){
        if(this.status.equals(status)){
            return true;
        }else {
            return false;
        }
    }
    public String getFlightNum(){
        return serviceObject.getServiceObject().getFlightNumber();
    }

    public String getFullFlightNumber(){
        return this.serviceObject.serviceObject.getAirline().iataCode + " " +  this.serviceObject.serviceObject.flightNumber;
    }

    //получение всех задач по одному рейсу (по номеру рейса)
    public boolean getOneFlightTasks(String flight){
        String str = this.serviceObject.serviceObject.getAirline().iataCode + " " +  this.serviceObject.serviceObject.flightNumber;
        if(str.equals(flight)){
            return true;
        }else {
            return false;
        }
    }
    public String getOperationList(Operations[] operation){
        String res = "";
        for(int i = 0; i < operation.length;i++){
            res += "[" +  operation[i].operationSpecification.name + "]";
        }
        return res;
    }

    private List<Operations> operationList(Operations[] operation) {
        for(int i = 0; i < operation.length;i++){
            listOper.add(operation[i]);
        }
        return listOper;
    }

    public List<Operations> getListOper() {
        return listOper;
    }

    //Получение информации по задаче id , status , flightNumber + resourceType + operationName + scheduledStart // operation[0].operationSpecification.name
    public String getInfo(){
        return id + " " + status + " " + getFullFlightNumber() + " " + resources.resourceType + " " + getOperationList(operation) + " " + scheduledStart;
    }
    public String getInfoLong(){
        String str = "===================" + "\n" +
                "Задача: " + "\n" +
                "id " + id + "\n" +
                "Cтатус " + status + "\n" +
                "Рейс " + getFullFlightNumber() + "\n" +
                "Тип ресурса " + resources.resourceType + "\n" +
                "Операции: " + getOperationList(operation) +
                "Начало выполнения задачи " + scheduledStart + "\n" +
                "=================";
        return str ;
    }



    // Получение объекта ресурс
    public Resources getResources() {
        return resources;
    }
    // Получение объекта операции
    public Operations[] getOperation() {
        return operation;
    }

    @Override
    public String toString() {
        return "TasksModel{" +
                "id='" + id + '\'' +
                ", status='" + status + '\'' +
                ", scheduledStart='" + scheduledStart + '\'' +
                ", serviceObject=" + serviceObject +
                ", resources=" + resources +
                ", operation=" + Arrays.toString(operation) +
                ", listOper=" + listOper +
                '}';
    }
}
