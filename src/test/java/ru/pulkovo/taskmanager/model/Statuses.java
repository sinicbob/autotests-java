package ru.pulkovo.taskmanager.model;

import java.util.*;

public class Statuses {
    private static List<String> array = new ArrayList<>();

    public Statuses(){

    }



    public List<String> getArray() {
        return array;
    }

    public static List<String> setArray(String str) {
        array.add(str);
        return array;
    }


    public Map<String, Integer> allValueStatus(){
        Map<String, Integer> result = new HashMap<>();
        for(String s : array){
            if(result.containsKey(s)){
                result.put(s, result.get(s)+1);
            }else{
                result.put(s, 1);
            }
        }
        result.put("ALL", array.size());
        return result;
    }

}
