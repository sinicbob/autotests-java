package ru.pulkovo.taskmanager.model;

import com.google.gson.annotations.SerializedName;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class ServiceStandardProv {
    @SerializedName("conditions")
    private Condition[] condition;
    private String id;
    @SerializedName("serviceStandard")
    private ServiceStandard standard;

    public ServiceStandardProv(){

    }

    public String getId() {
        return id;
    }

    public Condition[] getCondition() {
        return condition;
    }

    public ServiceStandard getStandards() {
        return standard;
    }

    public static class Condition {
        private String id;
        public Obj object;
        private String type;
        @SerializedName("emptyType")
        private String emptyType;

        public Condition(){
            if(object == null){
                 this.object = new Obj();
            }
        }


//        private String con1;
//        private String con2;

        private String strCon;
        private String strVal;
        private String iSCon;
        private List iSVal;

//        public Condition() {
//            if(this.type == "InStringCondition"){
//                this.con1 = this.iSCon;
//                this.con2 = this.iSVal;
//
//            }
//        }

        public Obj getObject() {
            return object;
        }

        @Override
        public String toString() {
            return "Condition{" +
                    "id='" + id + '\'' +
                    ", object=" + object +
                    ", type='" + type + '\'' +
                    ", emptyType='" + emptyType + '\'' +
                    ", strCon='" + strCon + '\'' +
                    ", strVal='" + strVal + '\'' +
                    ", iSCon='" + iSCon + '\'' +
                    ", iSVal=" + iSVal +
                    '}';
        }
    }
    public static class Obj {
        private String objectName;
        private String propertyName;
        private String propertyType;

        public String getObjectName() {
            return objectName;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public String getPropertyType() {
            return propertyType;
        }

    }

}


