package ru.pulkovo.taskmanager.created_excel;

import com.google.gson.Gson;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.json.JSONArray;
import org.json.JSONObject;
import ru.pulkovo.taskmanager.manager.Base;
import ru.pulkovo.taskmanager.manager.Query;
import ru.pulkovo.taskmanager.model.TasksModel;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static ru.pulkovo.taskmanager.manager.Query.LIGHT_TASKS;
import static ru.pulkovo.taskmanager.manager.RequastInfo.postRequast;
import static ru.pulkovo.taskmanager.manager.RequastInfo.responseToString;


public class tasksExcelCreation implements Query {
    private static Base app;

    public static void main(String[] args) {

        HSSFWorkbook workbook = new HSSFWorkbook();

        HSSFSheet sheet = workbook.createSheet("tasks");

        List<TasksModel> list = fillData();

        int rowNum = 0;

        Row row = sheet.createRow(rowNum);
        row.createCell(0).setCellValue("ID");
        row.createCell(1).setCellValue("Статус");
        row.createCell(2).setCellValue("Номер рейса");
        row.createCell(3).setCellValue("Тип ресурса");


        for (TasksModel model : list) {
            createSheetHeader(sheet, ++rowNum, model);
        }

        try (FileOutputStream out = new FileOutputStream("src/main/java/excel/jsonFiles/tasks.xls")) {
            workbook.write(out);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Excel файл успешно создан!");
    }

    private static void createSheetHeader(HSSFSheet sheet, int rowNum, TasksModel model) {
        Row row = sheet.createRow(rowNum);

        row.createCell(0).setCellValue(model.getId());
        row.createCell(1).setCellValue(model.getStatus());
        row.createCell(2).setCellValue(model.getServiceObject().getServiceObject().getFlightNumber());
        row.createCell(3).setCellValue(model.getResources().getResourceType());

    }


    private static List<TasksModel> fillData() {
        List<TasksModel> data = new ArrayList<>();

        JSONObject json = responseToString(postRequast(TEST, LIGHT_TASKS, app.req().createVariablesDay("2021", "08", "19")).body().toString());
        JSONArray tasks = json.getJSONObject("data").getJSONArray("tasks");

        Gson gson = new Gson();

        for(int i = 0; i < tasks.length();i++) {
            JSONObject j = (JSONObject) tasks.get(i);
            TasksModel response = gson.fromJson(String.valueOf(j), TasksModel.class);
            data.add(new TasksModel(response.getId(), response.getStatus(), response.getServiceObject().getServiceObject().flightNumber, response.getResources().resourceType));
        }


        return data;
    }
}


