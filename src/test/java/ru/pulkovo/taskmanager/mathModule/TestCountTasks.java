package ru.pulkovo.taskmanager.mathModule;

import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.pulkovo.taskmanager.manager.Query;
import ru.pulkovo.taskmanager.manager.TestBase;
import ru.pulkovo.taskmanager.model.TasksModel;

import java.util.*;

import static ru.pulkovo.taskmanager.manager.RequastInfo.*;

public class TestCountTasks extends TestBase implements Query {

    @Test
    public void testCountTaskForFlight_WZ5074(){
        JSONObject res =  responseToString(postRequast(TEST_MATH,NEW_TASKS,createVariablesDay("2021","10","06")).body().toString());
        JSONArray tasks = res.getJSONObject("data").getJSONArray("tasks");
        //Create list all tasks in the date.
        List<TasksModel> task = searchTasks(tasks);

        Map<String,Integer> countTasks = new HashMap<>();
            for(TasksModel t : task){
                if(t.getOneFlightTasks("WZ 5074")){
                    if(countTasks.containsKey(t.getResources().resourceType)){
                        countTasks.put(t.getResources().resourceType,countTasks.get(t.getResources().resourceType) + 1);
                    }else {
                        countTasks.put(t.getResources().resourceType, 1);
                    }
                }
            } //
        Map<String,Integer> assertCountTasks = new HashMap<>();
        assertCountTasks.put("Employee",33);
        assertCountTasks.put("WasteDisposalMachine",1);
        assertCountTasks.put("Car",1);
        assertCountTasks.put("Stepladder",2);
        Assert.assertEquals(countTasks,assertCountTasks);

    }

    @Test
    public void testCountTaskForFlight_5N148(){
        JSONObject res =  responseToString(postRequast(TEST_MATH,NEW_TASKS,createVariablesDay("2021","10","06")).body().toString());
        JSONArray tasks = res.getJSONObject("data").getJSONArray("tasks");
        //Create list all tasks in the date.
        List<TasksModel> task = searchTasks(tasks);
        Map<String,Integer> countTasks = new HashMap<>();
        for(TasksModel t : task){
            if(t.getOneFlightTasks("5N 148")){
                if(countTasks.containsKey(t.getResources().resourceType)){
                    countTasks.put(t.getResources().resourceType,countTasks.get(t.getResources().resourceType) + 1);
                }else {
                    countTasks.put(t.getResources().resourceType, 1);
                }
            }
        }
        Map<String,Integer> assertCountTasks = new HashMap<>();
        assertCountTasks.put("Employee",5);
        assertCountTasks.put("Car",1);
        Assert.assertEquals(countTasks,assertCountTasks);
    }




    public static List<TasksModel> searchTasks(JSONArray x){
        List<TasksModel> tasks = new ArrayList<>();
        for(int i = 0; i < x.length(); i++){
            Gson gson = new Gson();
            JSONObject j = (JSONObject) x.get(i);
            TasksModel response = gson.fromJson(String.valueOf(j), TasksModel.class);
            tasks.add(response);
        }
        return tasks;

    }
}
