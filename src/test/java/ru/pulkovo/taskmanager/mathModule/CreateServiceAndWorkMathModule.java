package ru.pulkovo.taskmanager.mathModule;

import com.google.gson.Gson;
import org.joda.time.DateTime;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.pulkovo.taskmanager.generators.FlightGenerator;
import ru.pulkovo.taskmanager.manager.Query;
import ru.pulkovo.taskmanager.manager.TestBase;
import ru.pulkovo.taskmanager.model.FlightModel;

import java.sql.SQLException;


public class CreateServiceAndWorkMathModule extends TestBase implements Query {

    @BeforeMethod
    public void verifyCreatedCustomService(){
//        //Создание сервиса
//        String cretedServiceId = String.valueOf(app.request("test-math").createService("S20_22 test").getJSONObject("data").getJSONObject("createServiceStandard").get("id"));
//        //Создание операции у сервиса который создан выше
//        String operationId = String.valueOf(app.request("test-math").createOperatinoToService(cretedServiceId).getJSONObject("data").getJSONObject("createOperationSpecification").get("id"));
//        //Создание у операции требования без ресурса.
//        String resourceSpecId = String.valueOf(app.request("test-math").createOperationSpecification(operationId,"2").getJSONObject("data").getJSONObject("createOperationResourceSpecification").get("id"));
//        // Добавление к созданому требования ресурса и заполнение требования к ресурсу у операции.
//        app.request("test-math").addedResourceRequirementToOperationResourceSpecification(resourceSpecId);
//        //Создание записи по созданному сервису в справочнике "Условия предоставления услуг"
//        String serviceStandartProvId = String.valueOf(app.request("test-math").createServiceStandardProviding(cretedServiceId,false).getJSONObject("data").getJSONObject("createServiceStandardProviding").get("id"));
//        // Добавление требования к созданному сервису в справочнике "Условия предоставление".
//        app.request("test-math").addedConditionToServiceStandardProviding(serviceStandartProvId);
//        // Создание для  service -а , serviceOffer -а
//        app.request("test-math").createServiceOffer(cretedServiceId);
    }

    @Test
    public void runMathModuleWithСustomFlight() throws SQLException, ClassNotFoundException {
        System.out.println("Действия по созданию рейса");
        FlightGenerator generator = new FlightGenerator();
        FlightModel flight = generator.generateOneFlight();
        flight.getServiceObject().setFlightNumber("260996");
        flight.getServiceObject().setArrivalDatetime(new DateTime(2021,12,24,17,0));
        System.out.println(new Gson().toJson(flight, FlightModel.class));
        System.out.println("Подключаемся к бд и отправляем туда рейс");
        String flightBD = flight.getServiceObject().toStringBD();
        app.db().createFlightDB(flightBD);
        System.out.println("Добавление рейса в подсчет");
        String reqRes = app.request("test-math").addedFlightToMathModule(flight);
        System.out.println(reqRes);
        System.out.println("Проверки по созданному рейсу");

    }


    @AfterMethod
    public void deletedCustomService(){

    }
}
