package ru.pulkovo.taskmanager.mathModule;

import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.pulkovo.taskmanager.manager.Query;
import ru.pulkovo.taskmanager.manager.TestBase;
import ru.pulkovo.taskmanager.model.TasksModel;

import java.io.FileWriter;
import java.io.IOException;
import java.net.http.HttpResponse;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static ru.pulkovo.taskmanager.manager.RequastInfo.*;


public class CheckResourceSlot extends TestBase implements Query {

    @Test
    public void testCheckSlots() throws IOException {
        HttpResponse request = postRequast(app.getProperty("api.test"), TASKS_SLOTS, createVariablesDayNow(createDateTimeNow())); // createVariablesDay("2021","11","23")
        JSONObject res = responseToString(request.body().toString());
        JSONArray tasks = res.getJSONObject("data").getJSONArray("tasks");

        List<TasksModel> task = searchTasks(tasks);

        FileWriter myWriter = new FileWriter("src/test/resources/badTasks.log"); // failedSlots_21-11-23
        AtomicInteger badTasks = new AtomicInteger();

        for(TasksModel t : task){
            t.getOperationResourceSlots().forEach(a -> a.getOperationResourceRequirements().forEach(b -> {
                if(b.getObject().getPropertyName().equals("resourceType")){
                    if(t.getResources().resourceType.equals(b.getValue())){

                    } else {
//                        badSlotsStatusNew.add(t.getId());
                        try {
                            myWriter.write("|Task_id| ( " + t.getId() + " ) [resourceSlot= " + b.getValue() + " != " + " resourceType= " + t.getResources().resourceType +" ]" + "\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        badTasks.getAndIncrement();
                    }
                }
            }));
        }
        float result = (badTasks.floatValue() / task.size()) * 100;
        myWriter.write("\n "+ createVariablesDayNow(createDateTimeNow()).replace("{\"filter\":{","{{{"));
        myWriter.write("\n Задач с неправильным ресурс слотом: " + badTasks + " Кол-во" +
                        " \n Всего задач: " + task.size() +
                "\n Процент задач с неправильным ресурс слотом " + result + " %" );
        myWriter.close();
        System.out.println("Задач с неправильным ресурс слотом: " + badTasks + " Кол-во" +
                " \n Всего задач: " + task.size() );
        System.out.println("Процент задач с неправильным ресурс слотом " + result + " %");
        System.out.println("Создан файл badTasks.log");
        Assert.assertEquals(badTasks.get(),0);
    }

    public static List<TasksModel> searchTasks(JSONArray x){
        List<TasksModel> tasks = new ArrayList<>();
        for(int i = 0; i < x.length(); i++){
            Gson gson = new Gson();
            JSONObject j = (JSONObject) x.get(i);
            TasksModel response = gson.fromJson(String.valueOf(j), TasksModel.class);
            tasks.add(response);
        }
        return tasks;

    }
}
