package ru.pulkovo.taskmanager.mathModule;

import com.google.gson.Gson;
import ru.pulkovo.taskmanager.manager.Query;
import ru.pulkovo.taskmanager.manager.TestBase;
import ru.pulkovo.taskmanager.model.OperationResourceSpecifications;
import ru.pulkovo.taskmanager.model.TasksModel;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static ru.pulkovo.taskmanager.manager.RequastInfo.*;

public class TaskRescheckSpecRes extends TestBase implements Query {

    @Test
    public void CheckResTask_SpecRes(){
        JSONObject res = responseToString(postRequast(TEST, NEW_TASKS, createVariablesDayNow(createDateTimeNow())).body().toString());
        JSONArray tasks = res.getJSONObject("data").getJSONArray("tasks");

        JSONObject specification = responseToString(postRequast(TEST,SPECIFICATION_RESOURCES,createDefaultVariables()).body().toString());
        JSONArray specRes = specification.getJSONObject("data").getJSONArray("operationResourceSpecifications");
        List<OperationResourceSpecifications> specs =  search(specRes);
        List<TasksModel> task = searchTasks(tasks);
        int badRes = 0;

        for (TasksModel t : task){
            String operation = t.getOperation()[0].operationSpecification.name;
//            String id = t.getOperation()[0].getOperationSpecification().getId();
            String resType = t.getResources().getResourceType();
//            String idSp = "";
            String resTypeSp = "";
            String operationSp = "";
            for (OperationResourceSpecifications sp : specs){
                operationSp = sp.getOperationSpecification().getName();
//                idSp = sp.getOperationSpecification().getId();
                for(OperationResourceSpecifications.ResourceRequirements rs : sp.getResourceRequirements()){
                    if(rs.getObject().getPropertyName().equals("resourceType")){
                        resTypeSp = rs.getStrVal();
                    }
                }
                if(operation.equals(operationSp)){
                    if(!resType.equals(resTypeSp)){
                        badRes++;
                        System.out.println(t.getId() + " resType != resTypeSp");
                    }
                }

            }
        }
        System.out.println(badRes);
    }



    public List<OperationResourceSpecifications> search(JSONArray x){
        List<OperationResourceSpecifications> specRes = new ArrayList<>();

        for(int i = 0; i < x.length();i++){
            Gson gson = new Gson();
            JSONObject j = (JSONObject) x.get(i);
            OperationResourceSpecifications response = gson.fromJson(String.valueOf(j), OperationResourceSpecifications.class);
            specRes.add(response);
        }
        return specRes;
    }

    public static List<TasksModel> searchTasks(JSONArray x){
        List<TasksModel> tasks = new ArrayList<>();
        for(int i = 0; i < x.length(); i++){
            Gson gson = new Gson();
            JSONObject j = (JSONObject) x.get(i);
            TasksModel response = gson.fromJson(String.valueOf(j), TasksModel.class);
            tasks.add(response);
        }
        return tasks;

    }






}
